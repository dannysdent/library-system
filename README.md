# Library System
Tailwind | Flask | SQLite

## Setup

Install dependencies & run server

```bash
# Install tailwind
npm install

# Install requirements
pip install -r requirements.txt

# Run server
python main.py runserver

```